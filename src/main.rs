use clap::Parser;
mod actions;
mod consts;
mod date;

/// "Your memory safe memory safe."
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Add a task due on date given by --date
    #[arg(short, long, default_value_t = String::from(""))]
    add: String,
    /// Date, used with -a or -p
    #[arg(long, default_value_t = String::from("tomorrow"))]
    date: String,
    /// Done: mark a line number as done
    #[arg(short, long, default_value_t = 0)]
    done: i64,
    /// Edit mode
    #[arg(short, long)]
    edit: bool,
    /// Install mode
    #[arg(short, long)]
    install: bool,
    /// grep: search for lines containing a string
    #[arg(short, long, default_value_t = String::from(""))]
    grep: String,
    /// grepv: search for lines not containing a string
    #[arg(long, default_value_t = String::from(""))]
    grepv: String,
    /// Show overdue tasks
    #[arg(short, long)]
    overdue: bool,
    /// Procrastinate a line number
    #[arg(short, long, default_value_t = 0)]
    procrastinate: i64,
    /// Show tasks due soon
    #[arg(short, long)]
    soon: bool,
    /// Show tasks due today
    #[arg(short, long)]
    today: bool,
    /// Show tasks due this week
    #[arg(short, long)]
    week: bool,
}

fn main() {
    let args = Args::parse();
    if args.add.len() != 0 {
        let _ = actions::add(args.add, args.date).expect("Add task failed");
        let _ = actions::clean_whitespace();
        let _ = actions::sort();
        actions::hook();
    } else if args.done != 0 {
        let _ = actions::done(args.done).expect("Done task failed");
        let _ = actions::clean_whitespace();
        let _ = actions::sort();
        actions::hook();
    } else if args.edit {
        actions::edit();
        let _ = actions::clean_whitespace();
        let _ = actions::sort();
        actions::hook();
    } else if args.grepv.len() != 0 {
        actions::grepv(args.grepv);
    } else if args.install {
        println!("Installing...");
    } else if args.overdue {
        actions::print_range("1 year ago", "yesterday");
    } else if args.soon {
        actions::print_range("today", "3 days");
    } else if args.today {
        actions::print_range("today", "today");
    } else if args.week {
        actions::print_range("today", "1 week");
    } else if args.procrastinate != 0 {
        let _ = actions::procrastinate(args.procrastinate, args.date);
        let _ = actions::clean_whitespace();
        let _ = actions::sort();
    } else {
        actions::grep(args.grep);
    }
}
