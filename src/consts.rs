// Relative to $HOME
const INSTALL_PATH: &str = "/.local/bin";

#[allow(dead_code)]
pub fn install_path() -> String {
    let home = std::env::var("HOME").expect("Err a9dd6: weird HOME var?");
    home + &String::from(INSTALL_PATH)
}

#[allow(dead_code)]
pub fn configfile_path() -> String {
    let home = std::env::var("HOME").expect("Err 6bfcc: weird HOME var?");
    home + &String::from(INSTALL_PATH) + "/mem.json"
}

pub fn textfile_path() -> String {
    let home = std::env::var("HOME").expect("Err cb7d2: weird HOME var?");
    home + &String::from(INSTALL_PATH) + "/mem.txt"
}
