use chrono::{prelude::NaiveDate, ParseError};
use std::{io, process::Command};

#[derive(Debug)]
pub enum ParseDateError {
    Io(io::Error),
    DateParse(ParseError),
}

pub fn parse_date(datestr: String) -> Result<String, ParseDateError> {
    match Command::new("date")
        .args(["-d", &datestr, "+%Y-%m-%d"])
        .output()
        .map_err(ParseDateError::Io)
    {
        Ok(output) => Ok(String::from(
            String::from_utf8(output.stdout).unwrap_or_default().trim(),
        )),
        Err(e) => Err(e),
    }
}

pub fn pretty_print_date(datestr: &str) -> Result<String, ParseDateError> {
    match NaiveDate::parse_from_str(datestr, "%F").map_err(ParseDateError::DateParse) {
        Ok(date) => Ok(date.format("%a, %b %d").to_string()),
        Err(e) => Err(e),
    }
}
