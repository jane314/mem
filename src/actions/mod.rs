use crate::{
    consts::{self, textfile_path},
    date::{parse_date, pretty_print_date},
};
use std::{
    fs::{File, OpenOptions},
    io::{self, BufRead, Error, Write},
    process::{Command, ExitStatus, Output, Stdio},
};

pub fn add(task: String, datestr: String) -> io::Result<()> {
    let datestr = match datestr.len() {
        0 => parse_date(String::from("today")).unwrap_or_default(),
        _ => parse_date(datestr).unwrap_or_default(),
    };
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .append(true)
        .open(consts::textfile_path())?;
    let mut w = io::BufWriter::new(file);
    writeln!(w, "{} {}", datestr, task)?;
    println!("Added task \"{task}\" on date {datestr}");
    Ok(())
}

pub fn clean_whitespace() -> Result<Output, Error> {
    Command::new("sed")
        .args([
            "-i",
            "-E",
            "-e",
            r"/^[[:space:]]*$/d",
            &consts::textfile_path(),
        ])
        .output()
}

pub fn done(linenum: i64) -> io::Result<ExitStatus> {
    let status = Command::new("sed")
        .args([
            "-i",
            "-e",
            &format!("{linenum}s/^/done /"),
            &consts::textfile_path(),
        ])
        .status()?;
    println!("Marked task {linenum} as done");
    Ok(status)
}

pub fn edit() {
    let editor = std::env::var("EDITOR").expect("Err 75ac8: weird EDITOR var?");
    if editor.len() == 0 {
        panic!("Err ca756: empty EDITOR var");
    }
    let _ = Command::new(editor)
        .arg(consts::textfile_path())
        .stderr(Stdio::inherit())
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .status();
}

pub fn grep(str: String) {
    let file = File::open(textfile_path()).expect("Err a9dd6: opening textfile");
    let mut i = 0;
    for line in io::BufReader::new(file).lines() {
        i += 1;
        match line {
            Ok(line) => {
                let (first, _) = line.split_once(' ').unwrap_or_default();
                if first.ne("done") && line.to_lowercase().contains(&str.to_lowercase()) {
                    println!("{i} {line}");
                }
            }
            Err(e) => eprintln!("{:?}", e),
        }
    }
}

pub fn grepv(str: String) {
    let file = File::open(textfile_path()).expect("Err a9dd6: opening textfile");
    let mut i = 0;
    for line in io::BufReader::new(file).lines() {
        i += 1;
        match line {
            Ok(line) => {
                let (first, _) = line.split_once(' ').unwrap_or_default();
                if first.ne("done") && (!line.to_lowercase().contains(&str.to_lowercase())) {
                    println!("{i} {line}");
                }
            }
            Err(e) => eprintln!("{:?}", e),
        }
    }
}

pub fn hook() {
    let editor = std::env::var("HOME").expect("Err 75ac8: weird HOME var?");
    if editor.len() == 0 {
        panic!("Err f4248: empty HOME var");
    }
    let _ = Command::new(editor + "/.local/bin/mem.hook")
        .stderr(Stdio::inherit())
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .status();
}

pub fn print_range(a: &str, b: &str) {
    let a = parse_date(String::from(a)).expect("Invalid date string");
    let b = parse_date(String::from(b)).expect("Invalid date string");
    let file = File::open(textfile_path()).expect("Err a9dd6: opening textfile");
    let mut i = 0;
    let mut new_date = String::from("");
    for line in io::BufReader::new(file).lines() {
        i += 1;
        match line {
            Ok(line) => {
                let (first, _) = line.split_once(' ').unwrap_or_default();
                if first.ge(&a) && first.le(&b) {
                    if new_date.ne(first) {
                        new_date = String::from(first);
                        if i > 1 {
                            print!("\n");
                        }
                        println!(
                            "{}",
                            pretty_print_date(first).unwrap_or("Date parse error".to_string())
                        );
                    }
                    println!("{i} {}", line.to_string());
                }
            }
            Err(e) => eprint!("{:?}", e),
        }
    }
}

pub fn procrastinate(linenum: i64, datestr: String) -> io::Result<ExitStatus> {
    let datestr = parse_date(datestr).expect("Invalid date string");
    let status = Command::new("sed")
        .args([
            "-i",
            "-e",
            &format!(
                "{linenum}{}{}/",
                r#"s/[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}/"#, &datestr
            ),
            &consts::textfile_path(),
        ])
        .status()?;
    println!("Procrastinated task {linenum} to date {datestr}");
    Ok(status)
}

pub fn sort() -> Result<Output, Error> {
    Command::new("ex")
        .args(["-s", "+%!sort", "-cxa", &consts::textfile_path()])
        .output()
}
