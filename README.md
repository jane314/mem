# mem

"Your memory safe memory safe."

## Overview

`mem` is a memory safe safe for your memories and TODO list. Its data all lies
in `$HOME/.local/bin/mem.txt`.

## Installing / Building

1. [Install Rust](https://www.rust-lang.org/learn/get-started).
1. `./install`

This will build and install `mem` to `$HOME/.local/bin`.

## Configuration

The hook `$HOME/.local/bin` is run upon every execution of mem that is not a
read-write operation.

## Examples

| Command                                    | Purpose                                                   |
| ------------------------------------------ | --------------------------------------------------------- |
| `mem -a "Do laundry" --date "Next monday"` | Add a task due on the specified date                      |
| `mem -e`                                   | Edit the `mem.txt` file manually.                         |
| `mem -g 'house'`                           | Find all tasks containing "house" (case-insensitive).     |
| `mem --grepv 'house'`                      | Find all tasks NOT containing "house" (case-insensitive). |
| `mem -o`                                   | Show overdue tasks.                                       |
| `mem -p 3 --date tomorrow`                 | Procrastinate line number 3 to tomorrow.                  |
| `mem -s`                                   | Show tasks due soon.                                      |
| `mem -t`                                   | Show tasks due today.                                     |

## Usage

```
Usage: mem [OPTIONS]

Options:
  -a, --add <ADD>      Add a task due on date given by --date [default: ]
  -d, --date <DATE>    Date, used with -a or -p [default: tomorrow]
  -e, --edit           Edit mode
  -i, --install        Install mode
  -g, --grep <GREP>    grep: search for lines containing a string [default: ]
      --grepv <GREPV>  grepv: search for lines not containing a string [default: ]
  -o, --overdue        Show overdue tasks
  -p, --procrastinate  Procrastinate a line number
  -s, --soon           Show tasks due soon
  -t, --today          Show tasks due today
  -w, --week           Show tasks due this week
  -h, --help           Print help
  -V, --version        Print version
```
